import React from 'react';
import { Layout } from 'antd';
import Header from './components/Header';
import Jumbotron from './components/Jumbotron';
import Services from './components/Services';
import About from './components/About';
import Principles from './components/Principles';
import Appeal from './components/Appeal';
import Feedback from './components/Feedback';
import License from './components/License';
import Politics from './components/Politics';
import Contacts from './components/Contacts';
import Footer from './components/Footer';
const { Content } = Layout;

const form = {
  head: 'Остались вопросы?',
  button: 'Узнать больше',
  theme: 'light'
};

function App() {
  return (
  <Layout>
    <Header/>
    <Content>
      <Jumbotron/>
      <Services/>
      <About/>
      <Principles/>
      <Appeal/>
      <Feedback />
      <License/>
      <Politics/>
      <Feedback
        head={form.head}
        button={form.button}
        theme={form.theme}
      />
      <Contacts/>
      <Feedback
      head={form.head}
      button={"Заказать звонок"}
      />
    </Content>
    <Footer/>
  </Layout>
  );
}

export default App;
