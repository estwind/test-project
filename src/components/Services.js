import React from 'react';
import {Row, Col} from 'antd';

function Services() {
  return (
  <section className="services" id="services">
    <div className="container">
      <h2 className="head">Наши<br/><span className="head_inner"></span>услуги</h2>
      <div className="services_subhead">В своей деятельности мы​ используем смарт-решения</div>
      <Row gutter={14}>
        <Col sm={24} md={12} className="services_col">
          <div className="services_item">Пакетные решения в части обращения с отходами 1-3 класса опасности (подбор
            кодов ФККО, разрешительная документация, транспортирование, размещение, утилизация или обезвреживание)
          </div>
        </Col>
        <Col sm={24} md={12} className="services_col">
          <div className="services_item">Управление обращением с отходами на предприятиях согласно экологическим
            политикам Заказчиков, включая разработку и реализацию инженерных решений
          </div>
        </Col>
        <Col sm={24} md={12} className="services_col">
          <div className="services_item services_item__dark">Управление объектами назначения отходов</div>
        </Col>
        <Col sm={24} md={12} className="services_col">
          <div className="services_item">Поставка и сертификация специальной тары в соответствии с требованиями ДОПОГ
            (МПОГ)
          </div>
        </Col>
        <Col>
          <div className="services_item">Представление и защита интересов в отношениях с компетентными органами при
            проведении мероприятий государственного экологического контроля, в ходе производств по делам об
            административных правонарушениях, а также в судах по спорам в области природоохранного законодательства
          </div>
        </Col>
      </Row>
      <button className="services_btn btn">Обратная связь</button>
    </div>
  </section>
  );
}

export default Services;