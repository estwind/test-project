import React from 'react';
import sprite from './../images/sprite-contacts.svg';
import map from './../images/map.png';

function Contacts() {
  return (
  <section className="contacts" id="contacts">
    <div className="container">
      <h2 className="head">Наши<br/><span className="head_inner"/>контакты</h2>
    </div>
    <div className="map">
      <img className="map_img" src={map} alt="Карта"/>
      <div className="container">
        <address className="contacts_content">
          <ul className="contacts_list">
            <li className="contacts_item">
              <svg className="contacts_icon">
                <use href={sprite + "#call"}></use>
              </svg>
              <span>+7(812) 900-53-10</span>
            </li>
            <li className="contacts_item">
              <svg className="contacts_icon">
                <use href={sprite + "#mail"}></use>
              </svg>
              <span>support@yandex.ru</span>
            </li>
            <li className="contacts_item">
              <svg className="contacts_icon">
                <use href={sprite + "#poi"}></use>
              </svg>
              <span>ул. Иркутская, д. 4 «А»,<br/>
              Санкт-Петербург, Россия</span>
            </li>
          </ul>
        </address>
      </div>
    </div>
  </section>
  )
}

export default Contacts;