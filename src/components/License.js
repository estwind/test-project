import React from 'react';
import {Row, Col} from 'antd';
import license01 from './../images/license01.jpg';
import license02 from './../images/license02.jpg';
import license03 from './../images/license03.jpg';
import license04 from './../images/license04.jpg';

function License() {
  return (
  <section className="license" id="license">
    <div className="container">
      <h2 className="head">О нашей<br/><span className="head_inner"></span>компании</h2>
      <Row className="license_row" gutter={15}>
        <Col xs={24} sm={12} lg={6}>
          <img src={license01} className="license_screen" alt="#"/>
        </Col>
        <Col xs={24} sm={12} lg={6}>
          <img src={license02} className="license_screen" alt="#"/>
        </Col>
        <Col xs={24} sm={12} lg={6}>
          <img src={license03} className="license_screen" alt="#"/>
        </Col>
        <Col xs={24} sm={12} lg={6}>
          <img src={license04} className="license_screen" alt="#"/>
        </Col>
      </Row>
    </div>
  </section>
  )
}

export default License;