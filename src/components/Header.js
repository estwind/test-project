import React from 'react';
import {Anchor} from 'antd';
import {ReactComponent as Logo} from './../images/logo.svg';

const {Link} = Anchor;
export default class Header extends React.Component {
  state = {
    toggle: false
  };
  Toggle = () => {
    this.setState({toggle: !this.state.toggle})
  };
  
  render() {
    return (
    <header className="header">
      <div className="header_content">
        <div className="header_logo">
          <Logo className="logo"/>
        </div>
        <div className={this.state.toggle ? "hamburger active" : "hamburger"} onClick={this.Toggle}>
          <div className="hamburger_box">
            <div className="hamburger_inner"></div>
          </div>
        </div>
        <div className={this.state.toggle ? "nav show-nav" : "nav"}>
          <Anchor affix={false} className="nav_list">
            <Link className="nav_link" href="#services" title="Услуги"/>
            <Link className="nav_link" href="#about" title="О компании"/>
            <Link className="nav_link" href="#license" title="Лицензии и разрешения"/>
            <Link className="nav_link" href="#politics" title="Политика компании"/>
            <Link className="nav_link" href="#contacts" title="Контакты"/>
          </Anchor>
        </div>
        <div className='contact'>
          <a className="contact_link" href="tel:+78120000000">+7(812) 000-00-00‬</a><br/>
          <a className="contact_link" href="mailto:support@yandex.ru">support@yandex.ru</a>
        </div>
      </div>
    </header>
    );
  }
}
