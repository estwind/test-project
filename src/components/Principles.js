import React from 'react';
import {Row, Col} from 'antd';
import sprite from './../images/sprite.svg';

function Principles() {
  return (
  <div className="principles">
    <div className="container">
      <h2 className="principles_head">Принципы нашей работы</h2>
      <Row gutter={14}>
        <Col sm={24} lg={12} className="principles_col">
          <div className="principles_item">
            <svg className="principles_icon">
              <use href={sprite + "#def"}></use>
            </svg>
            <p className="principles_text">Прозрачны, официальны <br/> и открыты</p>
          </div>
        </Col>
        <Col sm={24} lg={12} className="principles_col">
          <div className="principles_item">
            <svg className="principles_icon">
              <use href={sprite + "#women"}></use>
            </svg>
            <p className="principles_text">Добросовестны и ответственны на всех этапах работы</p>
          </div>
        </Col>
        <Col sm={24} lg={12} className="principles_col">
          <div className="principles_item">
            <svg className="principles_icon">
              <use href={sprite + "#men"}></use>
            </svg>
            <p className="principles_text">Наш персонал — один из самых квалифицированных в СЗФО</p>
          </div>
        </Col>
        <Col sm={24} lg={12} className="principles_col">
          <div className="principles_item">
            <svg className="principles_icon">
              <use href={sprite + "#medal"}></use>
            </svg>
            <p className="principles_text">Постоянно обновляемая лицензия, десятки аттестатов и свидетельств</p>
          </div>
        </Col>
        <Col span={24}>
          <div className="principles_item">
            <svg className="principles_icon">
              <use href={sprite + "#list"}></use>
            </svg>
            <p className="principles_text">Соблюдаем федеральное и региональное законодательство,<br/>  требования заказчиков и стандарты ISO</p>
          </div>
        </Col>
      </Row>
    
    
    </div>
  </div>
  );
}

export default Principles;