import React from 'react';

function Jumbotron() {
  return (
  <div className="jumbotron">
    <div className="container">
      <h2 className="jumbotron_head">Нам стоит поручить<br/>обращение с отходами<br/>1-3 класса опасности</h2>
      <button className="jumbotron_btn btn">Заказать звонок</button>;
    </div>
  </div>
  );
}

export default Jumbotron;