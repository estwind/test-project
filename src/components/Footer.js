import React from 'react';

function Footer() {
  return (
  <footer className="footer">
    <div className="container">
      <div className="footer_content">
        <a className="footer_politic" href="#">Политика конфиденциальности</a>
        <div className="footer_copyright">&copy; 2020 Спецперевозчик. Все права защищены.</div>
      </div>
    </div>
  </footer>
  );
}

export default Footer;