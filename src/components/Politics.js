import React from 'react';

function Politics() {
  return (
  <section className="politics" id="politics">
    <div className="container">
      <h2 className="head">О нашей<br/><span className="head_inner"></span>компании</h2>
      <ul className="politics_list">
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Политика в области ОТ и ПБ<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Антикоррупционная политика<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Политика в области ОТ и ПБ<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Кодекс корпоративной этики<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Политика обработки персональных данных<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Политика в области дарения подарков и гостеприимства<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
        <li className="politics_item">
          <i className="politics_icon"></i>
          <p className="politics_desc">
            Политика ИСМ<br/>
            <a className="politics_link" href="#" target="_blank">Открыть</a>
          </p>
        </li>
      </ul>
    </div>
  </section>
  )
}

export default Politics;