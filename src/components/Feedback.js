import React from 'react';
import {Form, Input, Button, Checkbox} from 'antd';


const Feedback = (props) => {
  
  const requiredMes = 'Это обязательное поле';
  
  return (
  <div className={'form ' + 'form__'  + (props.theme || 'dark')}>
    <div className="container">
      <Form className="form_content"
            name="feedback"
            initialValues={{agree: true}}>
        <div className="form_fields">
          <h3 className="form_head">{props.head || 'У вас есть вопросы?'}</h3>
          <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: requiredMes,
            },
          ]}
          >
            <Input className="form_input" placeholder="ФИО"/>
          </Form.Item>
          
          <Form.Item
          name="tel"
          rules={[
            {
              required: true,
              message: requiredMes,
            },
          ]}
          >
            <Input className="form_input" placeholder="Номер телефона"/>
          </Form.Item>
          <Form.Item>
            <Button className="form_button" htmlType="submit">
              {props.button || 'Получить консультацию'}
            </Button>
          </Form.Item>
        </div>
        
        <Form.Item className="form_agree" name="agree" valuePropName="checked"
                   rules={[
                     { validator:(_, value) => value ? Promise.resolve() : Promise.reject('Should accept agreement') }
                   ]}>
          <Checkbox className="form_checkbox">Я даю согласие на обработку своих персональных данных и соглашаюсь с <a
          href="#" className="form_link" target="_blank">политикой
            конфиденциальности</a>
          </Checkbox>
        </Form.Item>
      </Form>
    </div>
  </div>
  );
};


export default Feedback;