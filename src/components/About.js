import React from 'react';
import {Row, Col} from 'antd';
import about from './../images/about.jpg';

function About() {
  return (
  <section className="about" id="about">
    <div className="container">
      <h2 className="head">О нашей<br/><span className="head_inner"></span>компании</h2>
      <Row gutter={31} className="about_row">
        <Col xs={24} md={8}>
          <img className="about_img" src={about} alt="Обращение с отходами"/>
        </Col>
        <Col  xs={24} md={16}>
          <ul className="about_list">
            <li className="about_item">
              <p className="about_text">Более 5 лет на рынке.</p></li>
            <li className="about_item">
              <p className="about_text">Лицензированы на работу по всему Федеральному классификационному каталогу отходов.
              </p></li>
            <li className="about_item">
              <p className="about_text">Транспортируем более 3000 видов отходов I-V классов опасности всех агрегатных состояний, кроме газов.
              </p></li>
            <li className="about_item">
              <p className="about_text">В оперативном управлении объект размещения отходов, включенный в государственный реестр (ГРОРО).
              </p></li>
            <li className="about_item">
              <p className="about_text">Для выполнения работ используем транспортные средства, оснащенные системами слежения «ЭРА-ГЛОНАСС» и
                спецаппаратурой согласно акту- альным требованиям ТР ТС «О безопасности колёсных транспортных средств»:
                илососы, малотоннажные фургоны, самосвалы, трассовые тягачи и полуприцепы.
              </p></li>
            <li className="about_item">
              <p className="about_text">Руководящий состав имеет международные статусы консультантов по вопросам перевозок опасных грузов,
                входит в Научно-технические советы Росприроднадзора, ФГУП «РосРАО» Госкорпорации «Росатом» и включен в
                профильный Комитет по экологии и природопользованию Российского союза промышленников и предпринимателей.
              </p></li>
            <li className="about_item">
              <p className="about_text">Работа на объектах назначения и система управления компанией организованы согласно стандартам ISO 9001,
                ISO 14001, ISO 45001.
              </p></li>
            <li className="about_item">
              <p className="about_text">Всемирно известная аудиторская компания EcoVadis в 2017 г. присудила и ежегодно подтверждает Silver
                Recognition Level в области социальной ответственности и охраны окружающей среды.
              </p></li>
          </ul>
        </Col>
      </Row>
      

    </div>
  </section>
  );
}

export default About;